module.exports = {
    platform: 'gitlab',
    endpoint: 'https://gitlab.com/api/v4/',
    token: process.env.GITLAB_TOKEN,

    repositories: ['bytestream/renovate-npm'],

    logLevel: 'debug',

    labels: ["dependencies"],

    requireConfig: true,
    onboarding: true,
    onboardingConfig: {
        extends: ['config:base'],
        prConcurrentLimit: 0,
        prHourlyLimit: 0,
        semanticCommits: true,
    },

    enabledManagers: [
        'npm',
        'composer'
    ],

    composerIgnorePlatformReqs: false,

    dependencyDashboard: true,
    major: {
        "dependencyDashboardApproval": true
    },
};
